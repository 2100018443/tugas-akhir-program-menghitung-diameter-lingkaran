.MODEL SMALL
.CODE
ORG 100H
START:
JMP PROSES

DATA:
ANGKA1 DB ?
ANGKA2 DB ?
HASIL  DB ?
PESAN1 DB 10,13,"MASUKKAN ANGKA 2                : $"
PESAN2 DB 10,13,"MASUKKAN JARI-JARI LINGKARAN    : $"
PESAN3 DB 10,13,"DIAMETER LINGKARAN              : $"

PROSES:

    LEA DX, PESAN1
    MOV AH, 9
    INT 21H
    
    MOV AH,1
    INT 21H
    SUB AL,30H
    MOV ANGKA1,AL


    LEA DX, PESAN2
    MOV AH,9
    INT 21H
    
    MOV AH, 1
    INT 21H
    SUB AL, 30H
    MOV ANGKA2, AL
    
    MOV AL, ANGKA1
    MOV BL, ANGKA2
    MUL BL
 
    PUSH AX
    LEA DX,PESAN3
    MOV AH,9
    INT 21H
    
    POP AX
    CALL INT_KE_STRING

    MOV AH,4CH
    INT 21H
    
    
INT_KE_STRING PROC     
      
    ;initilize count 
    mov cx,0h
    mov dx,0h 
    label1: 
        ; if ax is zero 
        cmp ax,0 
        je print1       
          
        ;initilize bx to 10 
        mov bx,10         
          
        ; extract the last digit 
        div bx                   
          
        ;push it in the stack 
        push dx               
          
        ;increment the count 
        inc cx               
          
        ;set dx to 0  
        xor dx,dx 
        jmp label1 
    print1: 
        ;check if count  
        ;is greater than zero 
        cmp cx,0 
        je exit
          
        ;pop the top of stack 
        pop dx 
          
        ;add 48 so that it  
        ;represents the ASCII 
        ;value of digits 
        add dx,48 
          
        ;interuppt to print a 
        ;character 
        mov ah,02h 
        int 21h 
          
        ;decrease the count 
        dec cx 
        jmp print1 
exit: 
ret 
INT_KE_STRING ENDP
    


ENDS
END START